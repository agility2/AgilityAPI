FROM python:latest

WORKDIR /app/agility_api

COPY . /app/agility_api/

RUN python3 -m pip install -r requirements.txt

CMD [ "python3",  "-m",  "flask", "run", "-h", "0.0.0.0" ]