# define the name of the virtual environment directory
VENV := myvirtual

# default target, when make executed without arguments
# all: myvirtual

$(VENV)/bin/activate: requirements.txt
	python3 -m venv $(VENV)
	./$(VENV)/bin/pip install -r requirements.txt

# myvirtual is a shortcut target
myvirtual: $(VENV)/bin/activate

run: myvirtual
	./$(VENV)/bin/pytest --disable-pytest-warnings
	./$(VENV)/bin/python3 runtime.py

clean:
	rm -rf $(VENV)
	find . -type f -name '*.pyc' -delete

.PHONY: all myvirtual run clean