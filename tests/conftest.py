import pytest
from application.app import factory
from application.models.tasks import CUser, db

pytest.global_token = ""
pytest.global_task_id = ""

db.drop_all()
app = factory()

@pytest.fixture()
def app_test():
    '''
    this function takes no argument
    This function rebuild the database after each performed test
    '''

    app.config.update({
        "TESTING": True,
    })

    yield app



@pytest.fixture()
def client(app_test):
    '''
    this function takes "app_test" has an argument
    This function return a pytest client instance
    '''
    return app_test.test_client()
