import pytest

def test_login(client):
    '''test token retreival'''
    reply = client.get(
        "/login",
        headers=dict(Authorization='Basic bWF0dGhpZXU6YXplcnR5MTIz'))
    pytest.global_token = reply.json['token']
    assert reply.status_code == 200
