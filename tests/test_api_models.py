import json
import pytest



def test_get_tasks_401(client):
    '''test an unauthentified request'''
    reply = client.get(
        "/v1/tasks",
        headers=dict(x_access_tokens="no token"))
    assert reply.status_code == 401


def test_post_tasks_201(client):
    '''test an post request with data loaded from an external json file'''
    token = 'Bearer ' + pytest.global_token
    with open("tests/samples/sample_post_201.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        reply = client.post(
            '/v1/tasks',
            headers=dict(Authorization=token),
            json=data
        )
        assert reply.status_code == 201


def test_get_tasks_200(client):
    '''test an get request'''
    token = 'Bearer ' + pytest.global_token
    reply = client.get(
        "/v1/tasks",
        headers=dict(Authorization=token))
    assert reply.status_code == 200


def test_post_422(client):
    '''test an post request with wrongly formated data,
    loaded from an external json file'''
    token = 'Bearer ' + pytest.global_token
    with open("tests/samples/sample_post_422.json", "r",  encoding="utf-8") as file:
        data = json.load(file)
        reply = client.post(
            '/v1/tasks',
            headers=dict(Authorization=token),
            json=data
        )
        assert \
            reply.status_code == 422 and \
            "semantic errors." in reply.text


def test_patch_200(client):
    '''test an patch request with data loaded from an external json file'''
    token = 'Bearer ' + pytest.global_token
    with open("tests/samples/sample_post_for_patch_201.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        reply = client.post(
            '/v1/tasks',
            headers=dict(Authorization=token),
            json=data
        )
        task_id = reply.json[0]['task_id']

    with open("tests/samples/sample_patch_200.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        reply = client.patch(
            f'/v1/tasks/{task_id}',
            headers=dict(Authorization=token),
            json=data
        )
        assert reply.status_code == 200
