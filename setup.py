'''
Environment variables used for the application execution
'''
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin

SQLALCHEMY_TRACK_MODIFICATIONS = True
SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/test4.db'
FLASK_DEBUG = False

APISPEC_SPEC =  APISpec(
    title='agility_api',
    version='v1',
    plugins=[MarshmallowPlugin()],
    openapi_version='2.0.0'
)

APISPEC_SWAGGER_URL = '/swagger/'
APISPEC_SWAGGER_UI_UR = '/swagger_ui/'
SECRET_KEY = 'magicpassw0Rd'
PROVIDE_AUTOMATIC_OPTIONS = False
