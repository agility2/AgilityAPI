# agility_api

## getting the source

```
git clone https://gitlab.com/agility2/agility_api.git
```

## Installation

This API can be installed by differents means. It can be run has a docker container, or directly on the user machine with the makefile

## makefile

```
cd agility_api
make run
```

This will :
- create the python virtual environment
- install the required python dependencies within the venv
- run the API in background
- execute the tests

## Dockerfile

```
cd agility_api
docker build -t agility_api:v1.0 .
docker run -it -d -p 5000:5000 agility_api:v1.0
```

This will build the docker image from the Dockerfile, and execute it with the port 5000 exposed on the user host

## Performing some more tests

A [postman](https://www.postman.com/downloads/) template is available in the tests/ directory. The user can use it to make custom test.

If postman can't be installed tests can be performed with cURL:
```
# token retrieval
curl --location --request GET 'http://localhost:5000/login' \
-u <user>:<password>'

# task list
curl --location --request GET 'http://localhost:5000/v1/tasks?last_update=2022-08-29' \
--header 'x-access-tokens: <token>'

# task creation
curl --location --request POST 'http://localhost:5000/v1/tasks' \
--header 'x-access-tokens: <token>' \
--header 'Content-Type: application/json' \
--data-raw '[
  {
    "description": "this is manually created task",
    "name": "testing",
    "priority": 1
  }
]'

# ...
```

Automatic tests can be executed through pytest :
**This will reinitialize the database**
```
cd agility_api
pytest --disable-pytest-warnings
```


## Notes
This application implement a model "Task" which parameters are :
* name : human readable name for the task
* priority : level of priority for the task. (1 is the highest priority)
* description : details/explanation for the task
* creation_date : date of creation, can't be updated
* last_update : date of the last update, must be updated any time the object is changed
* task_id : unique identifier for the task

Thank to the Flask class-based views this application allows to 
- create/read group of tasks
- read/update/delete individual task

Filters can be applied to read operation on group of tasks:
- priority : If this parameter if used, the reply will contain only task for the value passed. uri will be something like : /v1/tasks?**priority=3**
- last_update : This parameter must be used as a *lower time limit*. uri will be something like : /v1/tasks?**last_update=2022-08-29**
- search : This parameter must be used individually. It allows to search for string entries in the tasks list.
  uri will be something like : /v1/tasks?priority=3&search=teapot

*priority* and *last_update* filters can be combined.

A Swagger Interface is availabe at : http://localhost:5000/swagger-ui/

A basic token mechanism has been implemented to protect all interactions with the Task objets. a token must me requested to **/login** uri with basic authentication method, and used as a value for **x-access-tokens** header.

A gitlab pipeline can be triggered to validate automatically the API.

A *lint* step has been added at the end of the pipeline.