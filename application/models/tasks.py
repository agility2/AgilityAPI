from flask import Flask, request, jsonify, make_response
from flask_sqlalchemy import SQLAlchemy
from marshmallow import Schema, fields
from functools import wraps
import jwt

app = Flask(__name__)
app.config.from_object('setup')
db = SQLAlchemy(app)


class Task(db.Model):

    name = db.Column(db.String(150), nullable=False)
    priority = db.Column(db.Integer)
    description = db.Column(db.String(500), nullable=False)
    creation_date = db.Column(db.DateTime, nullable=False)
    last_update = db.Column(db.DateTime, nullable=False)
    task_id = db.Column(db.String(500), primary_key=True)


class TaskSchemaRaw(Schema):

    description = fields.Str()
    name = fields.Str()
    priority = fields.Integer()


class TaskSchema(Schema):

    name = fields.String()
    priority = fields.Integer()
    description = fields.String()    
    creation_date = fields.String()
    last_update = fields.String()
    task_id = fields.String()
    id = fields.String()

    class Meta:
        type_ = 'task'
        fields = (
            'name',
            'priority',
            'description',
            'creation_date',
            'last_update',
            'task_id',
            'id')


class CUser(db.Model):

    name = db.Column(db.String)
    client_id = db.Column(db.String(30), primary_key=True)
    client_secret = db.Column(db.String(30))

    def token_required(view):
        @wraps(view)
        def decorator(*args, **kwargs):
            token = None
            if 'Authorization' in request.headers:
                token = request.headers['Authorization'].split(' ')[1]
            if not token:
                return make_response(
                    jsonify(
                        {'error': 'you must specify a token in Authorization header'}
                    ), 401)

            try:
                data = jwt.decode(token, app.config['SECRET_KEY'], algorithms=['HS256'])
                current_user = CUser.query.filter_by(client_id=data['client_id']).first()

            except Exception as e:
                return make_response(
                    jsonify(
                        {'error': str(e)}
                ), 401)

            return view(*args, **kwargs)
        return decorator