'''
Define class-based view's for the api.

Classes:

    EngineItem
    EngineList

Functions:

    EngineItem.get(task_id)
    EngineItem.patch(task_id)
    EngineItem.delete(task_id)
    EngineList.get(parameters)
    EngineList.post()

'''

import uuid
import datetime
from flask import Flask, jsonify, make_response
from flask_apispec import use_kwargs, marshal_with
from flask_apispec.annotations import doc
from flask_apispec.views import MethodResource
from webargs import fields
from sqlalchemy import not_
from application.models.tasks import Task, TaskSchema, TaskSchemaRaw, CUser, db


app = Flask(__name__)
task_schema = TaskSchema()

class EngineItem(MethodResource):
    """
    Handling of individual task item.

    ...

    Methods
    -------
    get(task_id):
        return an individual task.
    delete(task_id):
        return a deletion success message
    patch(task_id):
        return an updated individual task
    """

    __name__ = "engine"

    @CUser.token_required
    @marshal_with(TaskSchema(many=False))
    def get(self, task_id):
        '''
        This function takes argument:
            task_id: this argument is retreived from the called url.
            It's a randomly generated uid,

        This function return an individual task, or a 404 message.
        '''
        single_task = Task.query.get_or_404(task_id)
        result = task_schema.dump(single_task)
        return result, 200


    @CUser.token_required
    def delete(self, task_id):
        '''
        This function takes argument:
            task_id: this argument is retreived from the called url.
            It's a randomly generated uid,

        This function return a deletion success message, or a 404 message.
        '''
        single_task = Task.query.get_or_404(task_id)
        db.session.delete(single_task)
        db.session.commit()
        message = f"task : {task_id} was deleted"
        return make_response(
            jsonify(
                {'message': message}
            ), 200)


    @use_kwargs(TaskSchemaRaw(many=False))
    @marshal_with(TaskSchema(many=False))
    @CUser.token_required
    def patch(self, task_id, **kwargs):
        '''
        This function takes argument:
            task_id: this argument is retreived from the called url.
            It's a randomly generated uid,

        This function return the modified task's attributes, or a 404 message.
        A condition prevents to take "task_id" or "cration_date" into account
        '''
        update_date = datetime.datetime.now()
        single_task = Task.query.get_or_404(task_id)
        for key, value in jsonify(kwargs).json.items():
            if key not in ('task_id', 'creation_date'):
                setattr(single_task, key, value)
        setattr(single_task, "last_update", update_date)
        db.session.commit()
        result = task_schema.dump(single_task)
        return result, 200



class EngineList(MethodResource):
    """
    Handling of task items.

    ...

    Methods
    -------
    get(**kwargs):
        takes optional parameters:
            last_update: dateTime formated value
            priority: integer value
        return an list of tasks.
    post():
        return a list of newly created tasks
    """

    @CUser.token_required
    @doc(tags=['tasks'], description='list already created tasks')
    @use_kwargs(
        {'last_update': fields.Str()},
        location="query",
        description="Must be used as a lower time limit")
    @use_kwargs(
        {'priority': fields.Str()},
        location="query",
        description="Must be used to filter on priorities")
    @use_kwargs(
        {'search': fields.Str()},
        location="query",
        description="Must be used to search for keyword occurences")
    @marshal_with(TaskSchema(many=True))
    def get(self, **kwargs):
        '''
        This function takes optional arguments:
            last_update: as a lower time limit
            priority: as a filter for priority value

        This function return list of tasks.
        The parameters can be used individually or together
        '''

        all_task = Task.query.all()
        if 'last_update' in kwargs:
            all_task = Task.query.filter(Task.last_update >= kwargs['last_update'])

        if 'priority' in kwargs:
            all_task = Task.query.filter(Task.priority == kwargs['priority'])

        if 'priority' in kwargs and 'last_update' in kwargs:
            all_task = Task.query.filter(
                Task.last_update >= kwargs['last_update']).filter(
                    Task.priority == kwargs['priority'])

        if 'search' in kwargs:
            all_task = []
            for item in Task.query.filter(Task.description.contains(kwargs['search'])).all():
                all_task.append(item)
            for item in Task.query.filter(
                Task.name.contains(kwargs['search'])).filter(
                    not_(Task.description.contains(kwargs['search']))).all():
                all_task.append(item)

        results = task_schema.dump(all_task, many=True)
        return results, 200


    @CUser.token_required
    @doc(tags=['tasks'], description='tasks creation')
    @use_kwargs(TaskSchemaRaw(many=True))
    @marshal_with(TaskSchema(many=True))
    def post(self, *args):
        '''
        This function takes optional arguments and a json data set

        This function return list of created tasks.
        A nested loop is used to parse the body's items, and to parse the item's attributes
        '''

        except_date = datetime.datetime.now()
        results = []
        for new_task in jsonify(args).json:
            result = {}
            for key, value in new_task.items():
                result[key] = value

            new_task = Task(
                name=result['name'],
                priority=result['priority'],
                description=result['description'],
                creation_date=except_date,
                last_update=except_date,
                task_id=uuid.uuid1().hex
            )
            db.session.add(new_task)
            results.append(Task.query.filter(Task.task_id == new_task.task_id).first())
        db.session.commit()


        results = task_schema.dump(results, many=True)
        return results, 201
