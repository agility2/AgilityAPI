'''
Initiate the API runtime and implement the token provider.
'''

import datetime
import jwt
import sqlalchemy
from flask import Flask, request, jsonify
from flask_apispec.extension import FlaskApiSpec
from flask_restful import Api
from application.models.tasks import CUser, db
from application.engine import EngineItem, EngineList

app = Flask("agility_api")
def factory():
    '''
    This function takes no argument
    This function return the configured application
    '''
    # initiate the application
    app.config.from_object('setup')

    # initiate the database and link it to the app
    db.create_all()
    db.init_app(app)

    # initite the api
    api = Api(app)

    # initiate the openapi documentation
    docs = FlaskApiSpec(app)

    # add the two resource types to the API
    api.add_resource(EngineList, '/v1/tasks')
    api.add_resource(EngineItem, '/v1/tasks/<task_id>')

    # add ressources to the openapi documentation
    docs.register(EngineList)
    docs.register(EngineItem)


    try:
        user = CUser(name="Matthieu", client_id="matthieu", client_secret="azerty123")
        db.session.add(user)
        db.session.commit()

    except sqlalchemy.exc.IntegrityError:
        print(" - user already exist")


    return app


@app.route('/login', methods=['GET'])
def login():
    '''
    This function takes no argument,
    This function return a token or an error message.

    It's first checking if authorization params are present in the original request
    Then if the password correspond to the database user entry,
    it return the token in the body of the reply.
    If any exception occurs, it raises the exception message in the body of the reply
    '''
    credentials = request.authorization

    if credentials.username is None or credentials.password is None:
        return jsonify(
            {'error': 'you must provide a login and a password'}
        ), 401

    cuser = CUser.query.filter(CUser.client_id == credentials.username).first()

    if cuser.client_secret == credentials.password:
        try:
            token = jwt.encode(
                {
                    'client_id': cuser.client_id,
                    'exp' : datetime.datetime.utcnow() + datetime.timedelta(minutes=60)
                },
                app.config['SECRET_KEY']
            )
            return jsonify(
                {'token' : token}
            ), 200

        except Exception as exc:
            return jsonify(
                {'error': str(exc)}
            ), 500
    return jsonify(
        {'error': 'provided credentials are invalid'}
    ), 401
