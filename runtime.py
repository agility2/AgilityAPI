from application.app import factory

app = factory()

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)
